class WalletTransaction < ActiveRecord::Base
  
  def self.total_wallet_transactions
    self.count
  end
  
  
  def self.income
    self.all.select {|t| t.Category_Type =="Income"}
  end

  def self.cost
    self.all.select {|t| t.Category_Type == "Cost"}
    
  end
  
  def self.wallet_amount
    costs = cost.reduce(0) {|sum, cost| sum + cost.Amount}
    incomes = income.reduce(0) {|sum, income| sum + income.Amount}
    incomes - costs
  end
  
  
  
    
end
