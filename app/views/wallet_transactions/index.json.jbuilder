json.array!(@wallet_transactions) do |wallet_transaction|
  json.extract! wallet_transaction, :id, :Category_Type, :Transaction_Description, :Amount
  json.url wallet_transaction_url(wallet_transaction, format: :json)
end
