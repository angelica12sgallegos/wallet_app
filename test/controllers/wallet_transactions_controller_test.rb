require 'test_helper'

class WalletTransactionsControllerTest < ActionController::TestCase
  setup do
    @wallet_transaction = wallet_transactions(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:wallet_transactions)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create wallet_transaction" do
    assert_difference('WalletTransaction.count') do
      post :create, wallet_transaction: { Amount: @wallet_transaction.Amount, Category_Type: @wallet_transaction.Category_Type, Transaction_Description: @wallet_transaction.Transaction_Description }
    end

    assert_redirected_to wallet_transaction_path(assigns(:wallet_transaction))
  end

  test "should show wallet_transaction" do
    get :show, id: @wallet_transaction
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @wallet_transaction
    assert_response :success
  end

  test "should update wallet_transaction" do
    patch :update, id: @wallet_transaction, wallet_transaction: { Amount: @wallet_transaction.Amount, Category_Type: @wallet_transaction.Category_Type, Transaction_Description: @wallet_transaction.Transaction_Description }
    assert_redirected_to wallet_transaction_path(assigns(:wallet_transaction))
  end

  test "should destroy wallet_transaction" do
    assert_difference('WalletTransaction.count', -1) do
      delete :destroy, id: @wallet_transaction
    end

    assert_redirected_to wallet_transactions_path
  end
end
