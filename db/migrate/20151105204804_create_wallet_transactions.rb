class CreateWalletTransactions < ActiveRecord::Migration
  def change
    create_table :wallet_transactions do |t|
      t.string :Category_Type
      t.text :Transaction_Description
      t.integer :Amount

      t.timestamps null: false
    end
  end
end
